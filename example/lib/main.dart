// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_gphoto/flutter_gphoto.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  String _platformVersion = 'Unknown';
  int someInt = 0;

  final FlutterGphoto _gPhoto = FlutterGphoto();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    FlutterGphoto.close();
  }

  @override
  Widget build(BuildContext context) {
    // _gPhoto = new FlutterGphoto();

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: <Widget>[
            Text('GPhoto2 Example:'),
            FlatButton(
              onPressed: () {
                FlutterGphoto.requestUsb();
              },
              color: Colors.blue,
              child: Text(
                "request USB (0, android)",
              ),
            ),
            FlatButton(
              onPressed: () {
                FlutterGphoto.open();
              },
              color: Colors.blue,
              child: Text(
                "open USB (1)",
              ),
            ),
            FlatButton(
              onPressed: () {
                // someInt = FlutterGphoto.init();
                // print('FlutterGphoto.init():\t\n' + someInt.toString());
                // someInt = FlutterGphoto.setCaptureTargetSdcard();
                // print('FlutterGphoto.setCaptureTargetSdcard():\t\n' +
                //     someInt.toString());
                // someInt = FlutterGphoto.triggerCamera();
                // print('FlutterGphoto.triggerCamera():\t' +
                //     someInt.toString() +
                //     "\n");

                // //FlutterGphoto.grabImage();

                // someInt = FlutterGphoto.setCaptureTargetInternalRam();
                // FlutterGphoto.exit();

                _gPhoto.initialize();
                _gPhoto.captureSD();
                print("initialized");
              },
              color: Colors.blue,
              child: Text(
                "init gphoto (2)",
              ),
            ),
            FlatButton(
              onPressed: () {
                _gPhoto.trigger();
              },
              color: Colors.blue,
              child: Text(
                "trigger camera (3)",
              ),
            ),
            FlatButton(
              onPressed: () {
                _gPhoto.captureInternal();
                _gPhoto.stop();
                FlutterGphoto.close();
              },
              color: Colors.blue,
              child: Text(
                "stop & close usb (4)",
              ),
            ),
            FlatButton(
              onPressed: () {
                //FlutterGphoto.open();
                _gPhoto.initialize();
                _gPhoto.trigger();
                print('FlutterGphoto.init() IOS:\t\n' + someInt.toString());
              },
              color: Colors.blue,
              child: Text(
                "trigger camera (2, IOS)",
              ),
            )
          ],
        ),
      ),
    );
  }
}
