// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import <Flutter/Flutter.h>
#import "CaptureManager.h"

@interface FlutterGphotoPlugin : NSObject<FlutterPlugin, CaptureManagerDelegate>

@property CaptureManager* manager;

@end
