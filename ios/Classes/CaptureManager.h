//
//  CaptureManager.h
//  GPhotoTestApp
//
//  Created by Jonas Scheer on 30.07.20.
//  Copyright © 2020 Jonas Scheer. All rights reserved.
//

#ifndef __CAPTURE_MANAGER_USB_IOS_H__
#define __CAPTURE_MANAGER_USB_IOS_H__

#import <Foundation/Foundation.h>
#import <ImageCaptureCore/ICDeviceBrowser.h>
#import <ImageCaptureCore/ICCameraDevice.h>
#include <gphoto2/gphoto2-camera.h>

NS_ASSUME_NONNULL_BEGIN


@protocol CaptureManagerDelegate <NSObject>
@required
- (void) cameraStarted;
@end


@interface CaptureManager : NSObject<ICDeviceBrowserDelegate, ICCameraDeviceDelegate> {
   // Delegate to respond back
   id <CaptureManagerDelegate> _delegate;
}

@property (nonatomic,strong) id delegate;

@property (nonatomic, strong) ICDeviceBrowser* deviceBrowser;
@property (strong) ICCameraDevice* camera;

-(void)startCamera;

@end

NS_ASSUME_NONNULL_END

#endif __CAPTURE_MANAGER_USB_IOS_H__
