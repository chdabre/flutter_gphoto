// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTERGPHOTO_TOOLS_H
#define FLUTTER_PLUGIN_FLUTTERGPHOTO_TOOLS_H

#ifdef __APPLE__
    //#include <tr1/cstdint>
#else
    #include <cstdint>
#endif

#ifdef __ANDROID__
//#include <jni.h>
#include <android/log.h>
#define  LOG_TAG    "GPHOTO2"
#endif

int gp_print(const char *fmt, ...)
{
    va_list arg;
    int done;
    va_start (arg, fmt);

#ifdef __ANDROID__
    done = __android_log_vprint(ANDROID_LOG_INFO, LOG_TAG, fmt, arg);
#else
    done = vfprintf (stdout, fmt, arg);
#endif
    va_end (arg);
    return done;
}

#endif // FLUTTER_PLUGIN_FLUTTERGPHOTO_FFI_H
