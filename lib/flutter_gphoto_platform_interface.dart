import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_gphoto_method_channel.dart';

abstract class FlutterGphotoPlatform extends PlatformInterface {
  /// Constructs a FlutterGphotoPlatform.
  FlutterGphotoPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterGphotoPlatform _instance = MethodChannelFlutterGphoto();

  /// The default instance of [FlutterGphotoPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterGphoto].
  static FlutterGphotoPlatform get instance => _instance;
  
  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterGphotoPlatform] when
  /// they register themselves.
  static set instance(FlutterGphotoPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
