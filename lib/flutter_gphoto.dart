// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:async';
// import 'package:threading/threading.dart';
import 'package:flutter/services.dart';
import 'dart:ffi'; // For FFI
import 'package:ffi/ffi.dart';
import 'dart:io'; // For Platform.isX
import 'dart:typed_data';

final DynamicLibrary libflutter_gphoto = Platform.isAndroid
    ? DynamicLibrary.open("libflutter_gphoto.so")
    : DynamicLibrary.process();

final int Function() gp_init = libflutter_gphoto
    .lookup<NativeFunction<Int64 Function()>>("gp_init")
    .asFunction();

final int Function() gp_exit = libflutter_gphoto
    .lookup<NativeFunction<Int64 Function()>>("gp_exit")
    .asFunction();

final int Function() gp_triggerCamera = libflutter_gphoto
    .lookup<NativeFunction<Int64 Function()>>("gp_triggerCamera")
    .asFunction();

final int Function() gp_setCaptureTargetSdcard = libflutter_gphoto
    .lookup<NativeFunction<Int64 Function()>>("gp_set_capturetarget_sdcard")
    .asFunction();

final int Function() gp_setCaptureTargetInternalRam = libflutter_gphoto
    .lookup<NativeFunction<Int64 Function()>>("gp_set_capturetarget_internal")
    .asFunction();

final int Function() getNativeGrabImage = libflutter_gphoto
    .lookup<NativeFunction<Uint64 Function()>>("getNativeGrabImage")
    .asFunction<int Function()>();

final int Function(Pointer<Pointer<Uint8>>) gp_grabImage = libflutter_gphoto
    .lookup<NativeFunction<Uint64 Function(Pointer<Pointer<Uint8>> data)>>(
        "gp_grabImage")
    .asFunction<int Function(Pointer<Pointer<Uint8>> data)>();

// Camera Settings

final int Function(Pointer<Utf8>, Pointer<Pointer<Utf8>>) gp_getCameraOption =
    libflutter_gphoto
        .lookup<
            NativeFunction<
                Uint64 Function(Pointer<Utf8> optionName,
                    Pointer<Pointer<Utf8>> value)>>("gp_getCameraOption")
        .asFunction<
            int Function(
                Pointer<Utf8> optionName, Pointer<Pointer<Utf8>> value)>();

final int Function(Pointer<Utf8>, Pointer<Utf8>) gp_setCameraOption =
    libflutter_gphoto
        .lookup<
            NativeFunction<
                Uint64 Function(Pointer<Utf8> optionName,
                    Pointer<Utf8> value)>>("gp_setCameraOption")
        .asFunction<
            int Function(Pointer<Utf8> optionName, Pointer<Utf8> value)>();

// int64_t gp_getCameraOptionCount(char* optionName)
final int Function(Pointer<Utf8>) gp_getCameraOptionCount = libflutter_gphoto
    .lookup<NativeFunction<Uint64 Function(Pointer<Utf8> optionName)>>(
        "gp_getCameraOptionCount")
    .asFunction<int Function(Pointer<Utf8> optionName)>();

// uint64_t gp_getCameraOptionChoice(char* optionName, char** value, int index);
final int Function(Pointer<Utf8>, Pointer<Pointer<Utf8>>, int)
    gp_getCameraOptionChoice = libflutter_gphoto
        .lookup<
            NativeFunction<
                Uint64 Function(
                    Pointer<Utf8> optionName,
                    Pointer<Pointer<Utf8>> value,
                    Uint64 index)>>("gp_getCameraOptionChoice")
        .asFunction<
            int Function(Pointer<Utf8> optionName, Pointer<Pointer<Utf8>> value,
                int index)>();

// uint64_t gp_getAllImageNames(char*** folderNames, char*** imgNames, uint64_t* imgCount);
final int Function(Pointer<Pointer<Pointer<Utf8>>>,
        Pointer<Pointer<Pointer<Utf8>>>, Pointer<Uint64>) gp_getAllImageNames =
    libflutter_gphoto
        .lookup<
            NativeFunction<
                Uint64 Function(
                    Pointer<Pointer<Pointer<Utf8>>> folderNames,
                    Pointer<Pointer<Pointer<Utf8>>> imagesNames,
                    Pointer<Uint64> imgCount)>>("gp_getAllImageNames")
        .asFunction<
            int Function(
                Pointer<Pointer<Pointer<Utf8>>> folderNames,
                Pointer<Pointer<Pointer<Utf8>>> imagesNames,
                Pointer<Uint64> imgCount)>();

final int Function(Pointer<Utf8>, Pointer<Utf8>, Pointer<Pointer<Uint8>>,
        Pointer<Uint64>, int, int) gp_getImageByName =
    libflutter_gphoto
        .lookup<
            NativeFunction<
                Uint64 Function(
                    Pointer<Utf8> folderName,
                    Pointer<Utf8> imagesName,
                    Pointer<Pointer<Uint8>> imgData,
                    Pointer<Uint64> imgCount,
                    Uint8 cameraFileType,
                    Uint32 copyData)>>("gp_getImageByName")
        .asFunction<
            int Function(
                Pointer<Utf8> folderName,
                Pointer<Utf8> imagesName,
                Pointer<Pointer<Uint8>> imgData,
                Pointer<Uint64> imgCount,
                int cameraFileType,
                int copyData)>();

class FlutterGphoto {
  // Thread _imgGrabThread;
  bool _isStreaming = false;

  void initialize() {
    gp_init();
  }

  void captureSD() {
    gp_setCaptureTargetSdcard();
  }

  void captureInternal() {
    gp_setCaptureTargetInternalRam();
  }

  void trigger() {
    gp_triggerCamera();
  }

  void stop() {
    gp_exit();
  }

  List<List<String>> getAllImageNames() {
    List<List<String>> resultList = [];
    List<String> folderNamesList = [];
    List<String> imageNamesList = [];
    resultList.add(folderNamesList);
    resultList.add(imageNamesList);

    Pointer<Pointer<Pointer<Utf8>>> folderNames =
        calloc.allocate<Pointer<Pointer<Utf8>>>(1);
    Pointer<Pointer<Pointer<Utf8>>> imageNames =
        calloc.allocate<Pointer<Pointer<Utf8>>>(1);
    Pointer<Uint64> imgCount = calloc.allocate<Uint64>(1);

    int ret = gp_getAllImageNames(folderNames, imageNames, imgCount);
    if (ret == 0) {
      for (int i = 0; i < imgCount.value; i++) {
        Pointer<Pointer<Utf8>> folder = folderNames.value.elementAt(i);
        String folderNamesStr = folder.value.toDartString();
        folderNamesList.add(folderNamesStr);

        Pointer<Pointer<Utf8>> img = imageNames.value.elementAt(i);
        String imageNameStr = img.value.toDartString();
        imageNamesList.add(imageNameStr);
      }
    }

    return resultList;
  }

  Uint8List getImageByName(String imgFolder, String imgName, int imgType) {
    Uint8List emptyBytes = Uint8List(0);

    Pointer<Utf8> folderUtf8 = StringUtf8Pointer(imgFolder).toNativeUtf8();
    Pointer<Utf8> imgUtf8 = StringUtf8Pointer(imgName).toNativeUtf8();

    Pointer<Pointer<Uint8>> data = calloc.allocate<Pointer<Uint8>>(1);
    Pointer<Uint64> imgSize = calloc.allocate<Uint64>(1);

    if (imgType < 0) return emptyBytes;

    int result =
        gp_getImageByName(folderUtf8, imgUtf8, data, imgSize, imgType, 0);
    if (result == 0) {
      data.value = calloc.allocate<Uint8>(imgSize.value);
      result =
          gp_getImageByName(folderUtf8, imgUtf8, data, imgSize, imgType, 1);
      if (result == 0) {
        Uint8List bytes = data.value.asTypedList(imgSize.value);
        return bytes;
      }
    }

    return emptyBytes;
  }

  List<String> getOptionChoises(String optionName) {
    List<String> choises = [];

    Pointer<Utf8> nameUtf8 = StringUtf8Pointer(optionName).toNativeUtf8();
    int ret = gp_getCameraOptionCount(nameUtf8);
    if (ret != 0) {
      Pointer<Pointer<Utf8>> value = calloc.allocate<Pointer<Utf8>>(1);
      value.value = calloc.allocate<Utf8>(128);

      for (int i = 0; i < ret; i++) {
        int retChoises = gp_getCameraOptionChoice(nameUtf8, value, i);
        if (retChoises > 0) {
          String valueStr = value.value.toDartString();
          choises.add(valueStr);
        }
      }
    }

    return choises;
  }

  void setOption(String optionName, String value) {
    Pointer<Utf8> nameUtf8 = StringUtf8Pointer(optionName).toNativeUtf8();
    Pointer<Utf8> valueUtf8 = StringUtf8Pointer(value).toNativeUtf8();

    int ret = gp_setCameraOption(nameUtf8, valueUtf8);

    return;
  }

  String getOption(String optionName) {
    Pointer<Utf8> nameUtf8 = StringUtf8Pointer(optionName).toNativeUtf8();
    Pointer<Pointer<Utf8>> data = calloc.allocate<Pointer<Utf8>>(1);
    data.value = calloc.allocate<Utf8>(128);

    int ret = gp_getCameraOption(nameUtf8, data);
    if (ret > 0) {
      String valueStr = data.value.toDartString();
      return valueStr;
    }

    return "0";
  }

  int getNativeGrabImageFunction() {
    return getNativeGrabImage();
  }

  static const MethodChannel _channel = const MethodChannel('flutter_gphoto');
  static const MethodChannel _callbackChannel =
      const MethodChannel('flutter_gphoto_callback');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<void> permissionCallback(Function(bool) callback) async {
    try {
      _callbackChannel.setMethodCallHandler((MethodCall call) async {
        switch (call.method) {
          case 'permissionCallback':
            callback(call.arguments);
            return;
          default:
            throw MissingPluginException();
        }
      });
    } on PlatformException catch (e) {
      // throw 'Error: ${e.message}';
      print('error: \permissionCallback():\t\n' + e.message!);
    }

    return;
  }

  static Future<bool> requestUsb() async {
    try {
      final String successMessage = await _channel.invokeMethod('requestUsb');
      print('requestUsb():\t\n' + successMessage);
      return true;
    } on PlatformException catch (e) {
      // throw 'Error: ${e.message}';
      print('error: \trequestUsb():\t\n' + e.message!);
    }

    return false;
  }

  static Future<bool> hasUsb() async {
    try {
      final String successMessage = await _channel.invokeMethod('hasUsb');
      print('hasUsb():\t\n' + successMessage);
      return true;
    } on PlatformException catch (e) {
      // throw 'Error: ${e.message}';
      print('error: \hasUsb() returned false.\t\n');
    }

    return false;
  }

  static Future<bool> open() async {
    try {
      final String successMessage = await _channel.invokeMethod('open');
      return true;
    } on PlatformException catch (e) {
      // throw 'Error: ${e.message}';
      print('error: \topen():\t\n' + e.message!);
    }

    return false;
  }

  static Future<bool> close() async {
    try {
      final String successMessage = await _channel.invokeMethod('close');
      return true;
    } on PlatformException catch (e) {
      print('error: \tclose():\t\n' + e.message!);
    }

    return false;
  }
}
