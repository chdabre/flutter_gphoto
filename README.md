# flutter_gphoto

Flutter plugin that uses the libgphoto2 cmake port (https://gitlab.com/gphoto/libgphoto_cmake).

## Getting Started

This project shows, how to use the libgphoto2 port (libgphoto2) in a flutter application.
So far, the code was tested on an android device with a Nikon Z6.

## IOS Setup
Add ` NSCameraUsageDescription` key to your `<mypp>/example/ios/Runner/Info.plist`.
```bash
<key> NSCameraUsageDescription</key>
<string>
    Uses external camera.
</string>
```

## Android
* usb access

# License
While this source is licensed under *BSD*, the used submodule [libgphoto_cmake](https://gitlab.com/gphoto/libgphoto_cmake),
is licensed under *GNU Lesser General Public License v2.1*.
